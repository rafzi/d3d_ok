#ifndef D3D_OK_FAKEVOLUMETEXTURE_H
#define D3D_OK_FAKEVOLUMETEXTURE_H

#include "FakeBaseTexture.h"


class FakeVolume;


class FakeVolumeTexture : public FakeBaseTexture
{
public:
    FakeVolumeTexture(IDirect3DDevice9 *pDevice, UINT width, UINT height, UINT depth, UINT levels, DWORD usage, D3DFORMAT format, D3DPOOL pool);

    STDMETHOD_(D3DRESOURCETYPE, GetType)(THIS) override;

    STDMETHOD(GetLevelDesc)(THIS_ UINT Level, D3DVOLUME_DESC *pDesc);
    STDMETHOD(GetVolumeLevel)(THIS_ UINT Level, IDirect3DVolume9** ppVolumeLevel);
    STDMETHOD(LockBox)(THIS_ UINT Level, D3DLOCKED_BOX* pLockedVolume, CONST D3DBOX* pBox, DWORD Flags);
    STDMETHOD(UnlockBox)(THIS_ UINT Level);
    STDMETHOD(AddDirtyBox)(THIS_ CONST D3DBOX* pDirtyBox);

    FakeVolume *m_fakeVolume;

};


class InternalVolumeTexture : public InternalUnknown
{
public:
    InternalVolumeTexture(FakeUnknown *host);

    ULONG AddRefInternal() override;
    ULONG ReleaseInternal() override;

};

#endif