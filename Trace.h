#ifndef D3D_OK_TRACE_H
#define D3D_OK_TRACE_H

//#define D3D_OK_ENABLE_TRACE

#ifdef D3D_OK_ENABLE_TRACE
#include <cstdio>
#define OK_TRACE(format, ...) printf("%s: ", __FUNCTION__); printf(format, __VA_ARGS__); printf("\n")
#else
#define OK_TRACE(format, ...)
#endif

#endif