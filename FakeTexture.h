#ifndef D3D_OK_FAKETEXTURE_H
#define D3D_OK_FAKETEXTURE_H

#include "FakeBaseTexture.h"


class FakeSurface;


class FakeTexture : public FakeBaseTexture
{
public:
    FakeTexture(IDirect3DDevice9 *pDevice, UINT width, UINT height, UINT levels, DWORD usage, D3DFORMAT format, D3DPOOL pool);

    STDMETHOD_(D3DRESOURCETYPE, GetType)(THIS) override;

    STDMETHOD(GetLevelDesc)(THIS_ UINT Level, D3DSURFACE_DESC *pDesc);
    STDMETHOD(GetSurfaceLevel)(THIS_ UINT Level, IDirect3DSurface9** ppSurfaceLevel);
    STDMETHOD(LockRect)(THIS_ UINT Level, D3DLOCKED_RECT* pLockedRect, CONST RECT* pRect, DWORD Flags);
    STDMETHOD(UnlockRect)(THIS_ UINT Level);
    STDMETHOD(AddDirtyRect)(THIS_ CONST RECT* pDirtyRect);

    FakeSurface *m_fakeSurface;

};


class InternalTexture : public InternalUnknown
{
public:
    InternalTexture(FakeUnknown *host);

    ULONG AddRefInternal() override;
    ULONG ReleaseInternal() override;

};

#endif