#ifndef D3D_OK_FAKEVERTEXBUFFER_H
#define D3D_OK_FAKEVERTEXBUFFER_H

#include "FakeResource.h"


class FakeVertexBuffer : public FakeResource
{
public:
    FakeVertexBuffer(IDirect3DDevice9 *pDevice, UINT size, DWORD usage, DWORD fvf, D3DPOOL pool);

    STDMETHOD_(D3DRESOURCETYPE, GetType)(THIS) override;

    STDMETHOD(Lock)(THIS_ UINT OffsetToLock, UINT SizeToLock, void** ppbData, DWORD Flags);
    STDMETHOD(Unlock)(THIS);
    STDMETHOD(GetDesc)(THIS_ D3DVERTEXBUFFER_DESC *pDesc);

    UINT m_size;
    DWORD m_usage;
    DWORD m_fvf;
    D3DPOOL m_pool;

};

#endif