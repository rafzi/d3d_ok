#include "FakeSurface.h"
#include "D3D_ok.h"


static UINT GetBitCountByFormat(D3DFORMAT fmtFormat)
{
    switch (fmtFormat)
    {
    case D3DFMT_DXT1:
        return 4;
    case D3DFMT_R3G3B2:
    case D3DFMT_A8:
    case D3DFMT_P8:
    case D3DFMT_L8:
    case D3DFMT_A4L4:
    case D3DFMT_DXT2:
    case D3DFMT_DXT3:
    case D3DFMT_DXT4:
    case D3DFMT_DXT5:
        return 8;
    case D3DFMT_X4R4G4B4:
    case D3DFMT_A4R4G4B4:
    case D3DFMT_A1R5G5B5:
    case D3DFMT_X1R5G5B5:
    case D3DFMT_R5G6B5:
    case D3DFMT_A8R3G3B2:
    case D3DFMT_A8P8:
    case D3DFMT_A8L8:
    case D3DFMT_V8U8:
    case D3DFMT_L6V5U5:
    case D3DFMT_D16_LOCKABLE:
    case D3DFMT_D15S1:
    case D3DFMT_D16:
    case D3DFMT_L16:
    case D3DFMT_INDEX16:
    case D3DFMT_CxV8U8:
    case D3DFMT_G8R8_G8B8:
    case D3DFMT_R8G8_B8G8:
    case D3DFMT_R16F:
        return 16;
    case D3DFMT_R8G8B8:
        return 24;
    case D3DFMT_A2W10V10U10:
    case D3DFMT_A2B10G10R10:
    case D3DFMT_A2R10G10B10:
    case D3DFMT_X8R8G8B8:
    case D3DFMT_A8R8G8B8:
    case D3DFMT_X8L8V8U8:
    case D3DFMT_Q8W8V8U8:
    case D3DFMT_V16U16:
        //case D3DFMT_W11V11U10: // Dropped in DX9.0 
    case D3DFMT_UYVY:
    case D3DFMT_YUY2:
    case D3DFMT_G16R16:
    case D3DFMT_D32:
    case D3DFMT_D24S8:
    case D3DFMT_D24X8:
    case D3DFMT_D24X4S4:
    case D3DFMT_D24FS8:
    case D3DFMT_D32F_LOCKABLE:
    case D3DFMT_INDEX32:
    case D3DFMT_MULTI2_ARGB8:
    case D3DFMT_G16R16F:
    case D3DFMT_R32F:
        return 32;
    case D3DFMT_A16B16G16R16:
    case D3DFMT_Q16W16V16U16:
    case D3DFMT_A16B16G16R16F:
    case D3DFMT_G32R32F:
        return 64;
    case D3DFMT_A32B32G32R32F:
        return 128;
    }
    return 0;
}

static UINT GetByteCountByFormat(D3DFORMAT format)
{
    return GetBitCountByFormat(format) / 8;
}

INT FakeSurface::GetPitchByFormat(D3DFORMAT format, UINT width)
{
    UINT block_width;
    UINT block_byte_count;

    switch (format)
    {
        // Block type formats.
    case D3DFMT_DXT1:
        block_width = 4;
        block_byte_count = 8;
        break;
    case D3DFMT_DXT2:
    case D3DFMT_DXT3:
    case D3DFMT_DXT4:
    case D3DFMT_DXT5:
        block_width = 4;
        block_byte_count = 16;
        break;
        //case D3DFMT_ATI1N:
        //case D3DFMT_ATI2N:
    case D3DFMT_YUY2:
    case D3DFMT_UYVY:
        block_width = 2;
        block_byte_count = 4;
        break;

        // Bitmap type formats.
    default:
        return width * GetByteCountByFormat(format);
    }

    return block_byte_count * ((width + block_width - 1) / block_width);
}



FakeSurface::FakeSurface(IDirect3DDevice9 *pDevice, IUnknown *pContainer, UINT width, UINT height, DWORD usage, D3DFORMAT format, D3DPOOL pool)
    : FakeResource(new InternalUnknown(this), pDevice)
{
    m_pContainer = pContainer;
    m_pitch = GetPitchByFormat(format, width);
    m_format = format;
    m_usage = usage;
    m_pool = pool;
    m_width = width;
    m_height = height;
}


ULONG FakeSurface::AddRef(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount+1);

    if (m_pContainer)
    {
        return m_pContainer->AddRef();
    }

    return FakeResource::AddRef();
}

ULONG FakeSurface::Release(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount-1);

    if (m_pContainer)
    {
        return m_pContainer->Release();
    }

    return FakeResource::Release();
}


D3DRESOURCETYPE FakeSurface::GetType(THIS)
{
    OK_TRACE("");

    return D3DRTYPE_SURFACE;
}

HRESULT FakeSurface::GetContainer(THIS_ REFIID riid, void** ppContainer)
{
    OK_TRACE("");

    if (riid == IID_FakeUnknown)
    {
        // For internal use to identify fake objects.
        // Don't write to out pointer and don't AddRef.
        return m_pContainer->QueryInterface(IID_FakeUnknown, nullptr);
    }
    if (!ppContainer)
    {
        return D3DERR_INVALIDCALL;
    }
    if (riid == IID_IDirect3DTexture9)
    {
        *ppContainer = m_pContainer;
        static_cast<IDirect3DTexture9*>(*ppContainer)->AddRef();
        return D3D_OK;
    }
    else if (riid == IID_IDirect3DCubeTexture9)
    {
        *ppContainer = m_pContainer;
        static_cast<IDirect3DCubeTexture9*>(*ppContainer)->AddRef();
        return D3D_OK;
    }

    return D3DERR_INVALIDCALL;
}

HRESULT FakeSurface::GetDesc(THIS_ D3DSURFACE_DESC *pDesc)
{
    OK_TRACE("");

    pDesc->Format = m_format;
    pDesc->Type = D3DRTYPE_SURFACE;
    pDesc->Usage = m_usage;
    pDesc->Pool = m_pool;
    pDesc->MultiSampleType = D3DMULTISAMPLE_NONE;
    pDesc->MultiSampleQuality = 1;
    pDesc->Width = m_width;
    pDesc->Height = m_height;
    return D3D_OK;
}

HRESULT FakeSurface::LockRect(THIS_ D3DLOCKED_RECT* pLockedRect, CONST RECT* pRect, DWORD Flags)
{
    OK_TRACE("");

    pLockedRect->Pitch = m_pitch;
    pLockedRect->pBits = D3D_ok::GetInstance()->getDummyMem();
    return D3D_OK;
}

HRESULT FakeSurface::UnlockRect(THIS)
{
    OK_TRACE("");

    return D3D_OK;
}

HRESULT FakeSurface::GetDC(THIS_ HDC *phdc)
{
    MessageBox(0, "FakeSurface::GetDC", "Not implemented", 0);
    *phdc = (HDC)0xdeadbeef;
    return D3D_OK;
}

HRESULT FakeSurface::ReleaseDC(THIS_ HDC hdc)
{
    OK_TRACE("");

    return D3D_OK;
}