#include "FakeVertexBuffer.h"
#include "D3D_ok.h"


FakeVertexBuffer::FakeVertexBuffer(IDirect3DDevice9 *pDevice, UINT size, DWORD usage, DWORD fvf, D3DPOOL pool)
    : FakeResource(new InternalUnknown(this), pDevice)
{
    m_size = size;
    m_usage = usage;
    m_fvf = fvf;
    m_pool = pool;
}


D3DRESOURCETYPE FakeVertexBuffer::GetType(THIS)
{
    OK_TRACE("");

    return D3DRTYPE_VERTEXBUFFER;
}


HRESULT FakeVertexBuffer::Lock(THIS_ UINT OffsetToLock, UINT SizeToLock, void** ppbData, DWORD Flags)
{
    OK_TRACE("");

    *ppbData = D3D_ok::GetInstance()->getDummyMem();
    return D3D_OK;
}

HRESULT FakeVertexBuffer::Unlock(THIS)
{
    OK_TRACE("");

    return D3D_OK;
}

HRESULT FakeVertexBuffer::GetDesc(THIS_ D3DVERTEXBUFFER_DESC *pDesc)
{
    OK_TRACE("");

    pDesc->Format = D3DFMT_VERTEXDATA;
    pDesc->Type = D3DRTYPE_VERTEXBUFFER;
    pDesc->Usage = m_usage;
    pDesc->Pool = m_pool;
    pDesc->Size = m_size;
    pDesc->FVF = m_fvf;
    return D3D_OK;
}