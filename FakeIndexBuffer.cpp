#include "FakeIndexBuffer.h"
#include "D3D_ok.h"


FakeIndexBuffer::FakeIndexBuffer(IDirect3DDevice9 *pDevice, UINT size, DWORD usage, D3DFORMAT format, D3DPOOL pool)
    : FakeResource(new InternalUnknown(this), pDevice)
{
    m_size = size;
    m_usage = usage;
    m_format = format;
    m_pool = pool;
}


D3DRESOURCETYPE FakeIndexBuffer::GetType(THIS)
{
    OK_TRACE("");

    return D3DRTYPE_INDEXBUFFER;
}


HRESULT FakeIndexBuffer::Lock(THIS_ UINT OffsetToLock, UINT SizeToLock, void** ppbData, DWORD Flags)
{
    OK_TRACE("");

    *ppbData = D3D_ok::GetInstance()->getDummyMem();
    return D3D_OK;
}

HRESULT FakeIndexBuffer::Unlock(THIS)
{
    OK_TRACE("");

    return D3D_OK;
}

HRESULT FakeIndexBuffer::GetDesc(THIS_ D3DINDEXBUFFER_DESC *pDesc)
{
    OK_TRACE("");

    pDesc->Format = m_format;
    pDesc->Type = D3DRTYPE_INDEXBUFFER;
    pDesc->Usage = m_usage;
    pDesc->Pool = m_pool;
    pDesc->Size = m_size;
    return D3D_OK;
}