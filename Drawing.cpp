#include "D3D_ok.h"
#include "Trace.h"


static HRESULT __stdcall hkDrawPrimitive(LPDIRECT3DDEVICE9 pDevice, D3DPRIMITIVETYPE PrimitiveType, UINT StartVertex, UINT PrimitiveCount)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkDrawIndexedPrimitive(LPDIRECT3DDEVICE9 pDevice, D3DPRIMITIVETYPE Type, INT BaseVertexIndex, UINT MinIndex, UINT NumVertices, UINT StartIndex, UINT PrimitiveCount)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkDrawPrimitiveUP(LPDIRECT3DDEVICE9 pDevice, D3DPRIMITIVETYPE PrimitiveType, UINT PrimitiveCount, const void *pVertexStreamZeroData, UINT VertexStreamZeroStride)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkDrawIndexedPrimitiveUP(LPDIRECT3DDEVICE9 pDevice, D3DPRIMITIVETYPE PrimitiveType, UINT MinVertexIndex, UINT NumVertices, UINT PrimitiveCount, const void *pIndexData, D3DFORMAT IndexDataFormat, const void *pVertexStreamZeroData, UINT VertexStreamZeroStride)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkDrawRectPatch(LPDIRECT3DDEVICE9 pDevice, UINT Handle, const float *pNumSegs, const D3DRECTPATCH_INFO *pRectPatchInfo)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkDrawTriPatch(LPDIRECT3DDEVICE9 pDevice, UINT Handle, const float *pNumSegs, const D3DTRIPATCH_INFO *pTriPatchInfo)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkClear(LPDIRECT3DDEVICE9 pDevice, DWORD Count, const D3DRECT *pRects, DWORD Flags, D3DCOLOR Color, float Z, DWORD Stencil)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkPresent(LPDIRECT3DDEVICE9 pDevice, const RECT *pSourceRect, const RECT *pDestRect, HWND hDestWindowOverride, const RGNDATA *pDirtyRegion)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkBeginScene(LPDIRECT3DDEVICE9 pDevice)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkEndScene(LPDIRECT3DDEVICE9 pDevice)
{
    OK_TRACE("");

    return D3D_OK;
}


bool D3D_ok::startDrawing(IDirect3DDevice9 *pDevice)
{
    auto drawp = m_hooker.hookVT(pDevice, 81, hkDrawPrimitive);
    auto drawip = m_hooker.hookVT(pDevice, 82, hkDrawIndexedPrimitive);
    auto drawpup = m_hooker.hookVT(pDevice, 83, hkDrawPrimitiveUP);
    auto drawipup = m_hooker.hookVT(pDevice, 84, hkDrawIndexedPrimitiveUP);
    auto drawrect = m_hooker.hookVT(pDevice, 115, hkDrawRectPatch);
    auto drawtri = m_hooker.hookVT(pDevice, 116, hkDrawTriPatch);
    auto clear = m_hooker.hookVT(pDevice, 43, hkClear);
    auto present = m_hooker.hookVT(pDevice, 17, hkPresent);
    auto begsc = m_hooker.hookVT(pDevice, 41, hkBeginScene);
    auto endsc = m_hooker.hookVT(pDevice, 42, hkEndScene);

    if (!drawp || !drawip || !drawpup ||
        !drawipup || !drawrect || !drawtri ||
        !clear || !present || !begsc ||
        !endsc)
    {
        return false;
    }

    return true;
}