#include "FakeVolumeTexture.h"
#include "FakeVolume.h"
#include "D3D_ok.h"


FakeVolumeTexture::FakeVolumeTexture(IDirect3DDevice9 *pDevice, UINT width, UINT height, UINT depth, UINT levels, DWORD usage, D3DFORMAT format, D3DPOOL pool)
    : FakeBaseTexture(new InternalVolumeTexture(this), pDevice, levels)
{
    m_fakeVolume = new FakeVolume(pDevice, (IUnknown*)this, width, height, depth, usage, format, pool);
    m_fakeVolume->m_internal = new InternalUnknown(m_fakeVolume);
}


D3DRESOURCETYPE FakeVolumeTexture::GetType(THIS)
{
    OK_TRACE("");

    return D3DRTYPE_VOLUMETEXTURE;
}


HRESULT FakeVolumeTexture::GetLevelDesc(THIS_ UINT Level, D3DVOLUME_DESC *pDesc)
{
    OK_TRACE("");

    return m_fakeVolume->GetDesc(pDesc);
}

HRESULT FakeVolumeTexture::GetVolumeLevel(THIS_ UINT Level, IDirect3DVolume9** ppVolumeLevel)
{
    OK_TRACE("");

    *ppVolumeLevel = (IDirect3DVolume9*)m_fakeVolume;
    (*ppVolumeLevel)->AddRef();
    return D3D_OK;
}

HRESULT FakeVolumeTexture::LockBox(THIS_ UINT Level, D3DLOCKED_BOX* pLockedVolume, CONST D3DBOX* pBox, DWORD Flags)
{
    OK_TRACE("");

    pLockedVolume->RowPitch = m_fakeVolume->m_rowPitch;
    pLockedVolume->SlicePitch = m_fakeVolume->m_slicePitch;
    pLockedVolume->pBits = D3D_ok::GetInstance()->getDummyMem();
    return D3D_OK;
}

HRESULT FakeVolumeTexture::UnlockBox(THIS_ UINT Level)
{
    OK_TRACE("");

    return D3D_OK;
}

HRESULT FakeVolumeTexture::AddDirtyBox(THIS_ CONST D3DBOX* pDirtyBox)
{
    OK_TRACE("");

    return D3D_OK;
}


InternalVolumeTexture::InternalVolumeTexture(FakeUnknown *host)
    : InternalUnknown(host)
{
}

ULONG InternalVolumeTexture::AddRefInternal()
{
    OK_TRACE("%u -> %u", m_internalRefCount, m_internalRefCount+1);

    auto host = (FakeVolumeTexture*)m_host;
    for (UINT i = 0; i < host->m_levels; i++)
    {
        host->m_fakeVolume->m_internal->AddRefInternal();
    }
    return InternalUnknown::AddRefInternal();
}

ULONG InternalVolumeTexture::ReleaseInternal()
{
    OK_TRACE("%u -> %u", m_internalRefCount, m_internalRefCount-1);

    auto host = (FakeVolumeTexture*)m_host;
    for (UINT i = 0; i < host->m_levels; i++)
    {
        host->m_fakeVolume->m_internal->ReleaseInternal();
    }
    return InternalUnknown::ReleaseInternal();
}