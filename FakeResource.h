#ifndef D3D_OK_FAKERESOURCE_H
#define D3D_OK_FAKERESOURCE_H

#include "FakeUnknown.h"
#include <d3d9.h>


class FakeResource : public FakeUnknown
{
public:
    FakeResource(InternalUnknown *internal_, IDirect3DDevice9 *pDevice);

    STDMETHOD_(ULONG, AddRef)(THIS) override;
    STDMETHOD_(ULONG, Release)(THIS) override;

    STDMETHOD(GetDevice)(THIS_ IDirect3DDevice9** ppDevice);
    STDMETHOD(SetPrivateData)(THIS_ REFGUID refguid, CONST void* pData, DWORD SizeOfData, DWORD Flags);
    STDMETHOD(GetPrivateData)(THIS_ REFGUID refguid, void* pData, DWORD* pSizeOfData);
    STDMETHOD(FreePrivateData)(THIS_ REFGUID refguid);
    STDMETHOD_(DWORD, SetPriority)(THIS_ DWORD PriorityNew);
    STDMETHOD_(DWORD, GetPriority)(THIS);
    STDMETHOD_(void, PreLoad)(THIS);
    STDMETHOD_(D3DRESOURCETYPE, GetType)(THIS);

    IDirect3DDevice9 *m_pDevice;
    DWORD m_priority = 0;

};

#endif