#ifndef D3D_OK_FAKECUBETEXTURE_H
#define D3D_OK_FAKECUBETEXTURE_H

#include "FakeBaseTexture.h"


class FakeSurface;


class FakeCubeTexture : public FakeBaseTexture
{
public:
    FakeCubeTexture(IDirect3DDevice9 *pDevice, UINT edgeLength, UINT levels, DWORD usage, D3DFORMAT format, D3DPOOL pool);

    STDMETHOD_(D3DRESOURCETYPE, GetType)(THIS) override;

    STDMETHOD(GetLevelDesc)(THIS_ UINT Level, D3DSURFACE_DESC *pDesc);
    STDMETHOD(GetCubeMapSurface)(THIS_ D3DCUBEMAP_FACES FaceType, UINT Level, IDirect3DSurface9** ppCubeMapSurface);
    STDMETHOD(LockRect)(THIS_ D3DCUBEMAP_FACES FaceType, UINT Level, D3DLOCKED_RECT* pLockedRect, CONST RECT* pRect, DWORD Flags);
    STDMETHOD(UnlockRect)(THIS_ D3DCUBEMAP_FACES FaceType, UINT Level);
    STDMETHOD(AddDirtyRect)(THIS_ D3DCUBEMAP_FACES FaceType, CONST RECT* pDirtyRect);

    FakeSurface *m_fakeSurface;

};


class InternalCubeTexture : public InternalUnknown
{
public:
    InternalCubeTexture(FakeUnknown *host);

    ULONG AddRefInternal() override;
    ULONG ReleaseInternal() override;

};

#endif