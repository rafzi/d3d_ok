# D3D_ok #

**Built with [hacklib](https://bitbucket.org/rafzi/hacklib).**

D3D_ok is a library that makes the DirectX 9 3D API do nothing. It can be used to significantly reduce resource usage of CPU, GPU, RAM and VRAM.

## Building ##

Clone this repository into the src/ subdirectory of [hacklib](https://bitbucket.org/rafzi/hacklib) and rerun CMake.