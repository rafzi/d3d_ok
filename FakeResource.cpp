#include "FakeResource.h"


FakeResource::FakeResource(InternalUnknown *internal_, IDirect3DDevice9 *pDevice)
    : FakeUnknown(internal_)
    , m_pDevice(pDevice)
{
}


ULONG FakeResource::AddRef(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount+1);

    auto refCount = InterlockedIncrement(&m_refCount);
    if (refCount == 1)
    {
        m_pDevice->AddRef();
        m_internal->AddRefInternal();
    }
    return refCount;
}

ULONG FakeResource::Release(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount-1);

    auto refCount = InterlockedDecrement(&m_refCount);
    if (refCount == 0)
    {
        auto pDevice = m_pDevice;

        m_internal->ReleaseInternal();
        pDevice->Release();
    }
    return refCount;
}


HRESULT FakeResource::GetDevice(THIS_ IDirect3DDevice9** ppDevice)
{
    OK_TRACE("");

    *ppDevice = m_pDevice;
    (*ppDevice)->AddRef();
    return D3D_OK;
}

HRESULT FakeResource::SetPrivateData(THIS_ REFGUID refguid, CONST void* pData, DWORD SizeOfData, DWORD Flags)
{
    MessageBox(0, "FakeResource::SetPrivateData", "Not implemented", 0);
    return D3D_OK;
}

HRESULT FakeResource::GetPrivateData(THIS_ REFGUID refguid, void* pData, DWORD* pSizeOfData)
{
    MessageBox(0, "FakeResource::GetPrivateData", "Not implemented", 0);
    return D3D_OK;
}

HRESULT FakeResource::FreePrivateData(THIS_ REFGUID refguid)
{
    MessageBox(0, "FakeResource::FreePrivateData", "Not implemented", 0);
    return D3D_OK;
}

DWORD FakeResource::SetPriority(THIS_ DWORD PriorityNew)
{
    OK_TRACE("%lu -> %lu", m_priority, PriorityNew);

    auto oldPrio = m_priority;
    m_priority = PriorityNew;
    return oldPrio;
}

DWORD FakeResource::GetPriority(THIS)
{
    OK_TRACE("%lu", m_priority);

    return m_priority;
}

void FakeResource::PreLoad(THIS)
{
    OK_TRACE("");
}

D3DRESOURCETYPE FakeResource::GetType(THIS)
{
    MessageBox(0, "FakeResource::GetType should not be able to be called. Missing override?", "Error", 0);
    return (D3DRESOURCETYPE)0;
}