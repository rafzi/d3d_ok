#ifndef D3D_OK_FAKEVOLUME_H
#define D3D_OK_FAKEVOLUME_H

#include "FakeUnknown.h"
#include <d3d9.h>


class FakeVolume : public FakeUnknown
{
public:
    FakeVolume(IDirect3DDevice9 *pDevice, IUnknown *pContainer, UINT width, UINT height, UINT depth, DWORD usage, D3DFORMAT format, D3DPOOL pool);

    STDMETHOD_(ULONG, AddRef)(THIS) override;
    STDMETHOD_(ULONG, Release)(THIS) override;

    STDMETHOD(GetDevice)(THIS_ IDirect3DDevice9** ppDevice);
    STDMETHOD(SetPrivateData)(THIS_ REFGUID refguid, CONST void* pData, DWORD SizeOfData, DWORD Flags);
    STDMETHOD(GetPrivateData)(THIS_ REFGUID refguid, void* pData, DWORD* pSizeOfData);
    STDMETHOD(FreePrivateData)(THIS_ REFGUID refguid);
    STDMETHOD(GetContainer)(THIS_ REFIID riid, void** ppContainer);
    STDMETHOD(GetDesc)(THIS_ D3DVOLUME_DESC *pDesc);
    STDMETHOD(LockBox)(THIS_ D3DLOCKED_BOX * pLockedVolume, CONST D3DBOX* pBox, DWORD Flags);
    STDMETHOD(UnlockBox)(THIS);

    IDirect3DDevice9 *m_pDevice;

    IUnknown *m_pContainer;
    INT m_rowPitch;
    INT m_slicePitch;
    D3DFORMAT m_format;
    DWORD m_usage;
    D3DPOOL m_pool;
    UINT m_width;
    UINT m_height;
    UINT m_depth;

};

#endif