#include "FakeUnknown.h"


FakeUnknown::FakeUnknown(InternalUnknown *internal_)
    : m_internal(internal_)
{
}


HRESULT FakeUnknown::QueryInterface(THIS_ REFIID riid, void** ppvObj)
{
    OK_TRACE("");

    if (riid == IID_FakeUnknown)
    {
        // For internal use to identify fake objects.
        // Don't write to out pointer and don't AddRef.
        return S_OK;
    }
    if (!ppvObj)
    {
        return E_POINTER;
    }
    if (riid == IID_IUnknown)
    {
        *ppvObj = static_cast<FakeUnknown*>(this);
        AddRef();
        return S_OK;
    }

    *ppvObj = NULL;
    return E_NOINTERFACE;
}

ULONG FakeUnknown::AddRef(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount+1);

    auto refCount = InterlockedIncrement(&m_refCount);
    if (refCount == 1)
    {
        m_internal->AddRefInternal();
    }
    return refCount;
}

ULONG FakeUnknown::Release(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount-1);

    auto refCount = InterlockedDecrement(&m_refCount);
    if (refCount == 0)
    {
        m_internal->ReleaseInternal();
    }
    return refCount;
}


InternalUnknown::InternalUnknown(FakeUnknown *host)
    : m_host(host)
{
}

ULONG InternalUnknown::AddRefInternal()
{
    OK_TRACE("%u -> %u", m_internalRefCount, m_internalRefCount+1);

    return InterlockedIncrement(&m_internalRefCount);
}

ULONG InternalUnknown::ReleaseInternal()
{
    OK_TRACE("%u -> %u", m_internalRefCount, m_internalRefCount-1);

    auto refCount = InterlockedDecrement(&m_internalRefCount);
    if (refCount == 0)
    {
        delete m_host;
        delete this;
    }
    return refCount;
}