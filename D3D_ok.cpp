#include "D3D_ok.h"
#include "FakeUnknown.h"


static const int PAGESIZE = 0x1000;


D3D_ok *g_instance = nullptr;


static HRESULT __stdcall hkReset(LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS *pPresentationParameters)
{
    OK_TRACE("");

    auto& currentStencil = D3D_ok::GetInstance()->m_currentStencil;
    auto& texStages = D3D_ok::GetInstance()->m_texStages;
    auto& currentIB = D3D_ok::GetInstance()->m_currentIB;

    // Clear all internally held resources.

    if (currentStencil != OK_GARBAGE_PTR && currentStencil && D3D_ok::IsFake(currentStencil))
    {
        ((FakeUnknown*)currentStencil)->m_internal->ReleaseInternal();
    }
    currentStencil = (IDirect3DSurface9*)OK_GARBAGE_PTR;
    for (auto& texStage : texStages)
    {
        if (texStage.second && D3D_ok::IsFake(texStage.second))
        {
            ((FakeUnknown*)texStage.second)->m_internal->ReleaseInternal();
        }
    }
    texStages.clear();

    if (currentIB != OK_GARBAGE_PTR && currentIB && D3D_ok::IsFake(currentIB))
    {
        ((FakeUnknown*)currentIB)->m_internal->ReleaseInternal();
    }
    currentIB = (IDirect3DIndexBuffer9*)OK_GARBAGE_PTR;

    auto orgFunc = D3D_ok::GetInstance()->m_hkReset->getLocation();
    return decltype(&hkReset)(orgFunc)(pDevice, pPresentationParameters);
}


D3D_ok *D3D_ok::GetInstance()
{
    return g_instance;
}

void *D3D_ok::AllocDummyMem(size_t size)
{
    // Align to page boundaries.
    int useSize = (size + PAGESIZE - 1) & ~(PAGESIZE - 1);
    int totalSize = useSize + PAGESIZE;
    auto dummyMem = _aligned_malloc(totalSize, PAGESIZE);
    if (!dummyMem)
    {
        return nullptr;
    }

    // Prevent corruption if size too small.
    DWORD oldProt = 0;
    if (!VirtualProtect((char*)dummyMem + useSize, PAGESIZE, PAGE_READWRITE | PAGE_GUARD, &oldProt))
    {
        _aligned_free(dummyMem);
        return nullptr;
    }

    return dummyMem;
}

void D3D_ok::FreeDummyMem(void *ptr)
{
    _aligned_free(ptr);
}

bool D3D_ok::IsFake(IUnknown *pUnknown)
{
    void *ptr;
    return pUnknown->QueryInterface(IID_FakeUnknown, &ptr) == D3D_OK;
}


D3D_ok::D3D_ok()
{
    g_instance = this;
}


void D3D_ok::setDummyMem(void *ptr)
{
    m_dummyMem = ptr;
}

void *D3D_ok::getDummyMem() const
{
    return m_dummyMem;
}





bool D3D_ok::startAll(IDirect3DDevice9 *pDevice)
{
    m_hkReset = m_hooker.hookVT(pDevice, 16, hkReset);

    if (!m_hkReset)
    {
        return false;
    }

    return
        startDrawing(pDevice) &&
        startTextures(pDevice) &&
        startBuffers(pDevice) &&
        startShaders(pDevice);
}



/*

> : todo
x : done

QueryInterface // 0
AddRef // 1
Release // 2

TestCooperativeLevel // 3
GetAvailableTextureMem // 4
EvictManagedResources // 5
GetDirect3D // 6
GetDeviceCaps // 7
GetDisplayMode // 8
GetCreationParameters // 9
SetCursorProperties // 10
SetCursorPosition // 11
ShowCursor // 12
CreateAdditionalSwapChain // 13
GetSwapChain // 14
GetNumberOfSwapChains // 15
x Reset // 16
x Present // 17
GetBackBuffer // 18
GetRasterStatus // 19
SetDialogBoxMode // 20
SetGammaRamp // 21
GetGammaRamp // 22
x CreateTexture // 23
x CreateVolumeTexture // 24
x CreateCubeTexture // 25
x CreateVertexBuffer // 26
x CreateIndexBuffer // 27
> CreateRenderTarget // 28
> CreateDepthStencilSurface // 29
x UpdateSurface // 30
x UpdateTexture // 31
GetRenderTargetData // 32
GetFrontBufferData // 33
StretchRect // 34
ColorFill // 35
> CreateOffscreenPlainSurface // 36
> SetRenderTarget // 37
> GetRenderTarget // 38
x SetDepthStencilSurface // 39
x GetDepthStencilSurface // 40
x BeginScene // 41
x EndScene // 42
x Clear // 43
> SetTransform // 44
> GetTransform // 45
> MultiplyTransform // 46
> SetViewport // 47
> GetViewport // 48
> SetMaterial // 49
> GetMaterial // 50
> SetLight // 51
> GetLight // 52
> LightEnable // 53
> GetLightEnable // 54
> SetClipPlane // 55
> GetClipPlane // 56
> SetRenderState // 57
> GetRenderState // 58
> CreateStateBlock // 59
> BeginStateBlock // 60
> EndStateBlock // 61
> SetClipStatus // 62
> GetClipStatus // 63
x GetTexture // 64
x SetTexture // 65
> GetTextureStageState // 66
> SetTextureStageState // 67
> GetSamplerState // 68
> SetSamplerState // 69
ValidateDevice // 70
> SetPaletteEntries // 71
> GetPaletteEntries // 72
> SetCurrentTexturePalette // 73
> GetCurrentTexturePalette // 74
> SetScissorRect // 75
> GetScissorRect // 76
> SetSoftwareVertexProcessing // 77
> GetSoftwareVertexProcessing // 78
SetNPatchMode // 79
GetNPatchMode // 80
x DrawPrimitive // 81
x DrawIndexedPrimitive // 82
x DrawPrimitiveUP // 83
x DrawIndexedPrimitiveUP // 84
ProcessVertices // 85
> CreateVertexDeclaration // 86
> SetVertexDeclaration // 87
> GetVertexDeclaration // 88
> SetFVF // 89
> GetFVF // 90
x CreateVertexShader // 91
x SetVertexShader // 92
> GetVertexShader // 93
> SetVertexShaderConstantF // 94
> GetVertexShaderConstantF // 95
> SetVertexShaderConstantI // 96
> GetVertexShaderConstantI // 97
> SetVertexShaderConstantB // 98
> GetVertexShaderConstantB // 99
> SetStreamSource // 100
> GetStreamSource // 101
> SetStreamSourceFreq // 102
> GetStreamSourceFreq // 103
x SetIndices // 104
x GetIndices // 105
x CreatePixelShader // 106
x SetPixelShader // 107
> GetPixelShader // 108
> SetPixelShaderConstantF // 109
> GetPixelShaderConstantF // 110
> SetPixelShaderConstantI // 111
> GetPixelShaderConstantI // 112
> SetPixelShaderConstantB // 113
> GetPixelShaderConstantB // 114
x DrawRectPatch // 115
x DrawTriPatch // 116
DeletePatch // 117
CreateQuery // 118

*/