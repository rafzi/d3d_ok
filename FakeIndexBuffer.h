#ifndef D3D_OK_FAKEINDEXBUFFER_H
#define D3D_OK_FAKEINDEXBUFFER_H

#include "FakeResource.h"


class FakeIndexBuffer : public FakeResource
{
public:
    FakeIndexBuffer(IDirect3DDevice9 *pDevice, UINT size, DWORD usage, D3DFORMAT format, D3DPOOL pool);

    STDMETHOD_(D3DRESOURCETYPE, GetType)(THIS) override;

    STDMETHOD(Lock)(THIS_ UINT OffsetToLock, UINT SizeToLock, void** ppbData, DWORD Flags);
    STDMETHOD(Unlock)(THIS);
    STDMETHOD(GetDesc)(THIS_ D3DINDEXBUFFER_DESC *pDesc);

    UINT m_size;
    DWORD m_usage;
    D3DFORMAT m_format;
    D3DPOOL m_pool;

};

#endif