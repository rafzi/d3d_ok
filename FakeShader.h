#ifndef D3D_OK_FAKESHADER_H
#define D3D_OK_FAKESHADER_H

#include "FakeUnknown.h"
#include <d3d9.h>


class FakeShader : public FakeUnknown
{
public:
    FakeShader(IDirect3DDevice9 *pDevice);

    STDMETHOD_(ULONG, AddRef)(THIS);
    STDMETHOD_(ULONG, Release)(THIS);

    STDMETHOD(GetDevice)(THIS_ IDirect3DDevice9** ppDevice);
    STDMETHOD(GetFunction)(THIS_ void*, UINT* pSizeOfData);

    IDirect3DDevice9 *m_pDevice;

};

#endif