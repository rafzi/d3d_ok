#include "D3D_ok.h"
#include "FakeVertexBuffer.h"
#include "FakeIndexBuffer.h"


static HRESULT __stdcall hkCreateVertexBuffer(LPDIRECT3DDEVICE9 pDevice, UINT Length, DWORD Usage, DWORD FVF, D3DPOOL Pool, IDirect3DVertexBuffer9 **ppVertexBuffer, HANDLE *pSharedHandle)
{
    OK_TRACE("len: %u", Length);

    auto pBuffer = new FakeVertexBuffer(pDevice, Length, Usage, FVF, Pool);
    pBuffer->AddRef();

    *ppVertexBuffer = (IDirect3DVertexBuffer9*)pBuffer;
    return D3D_OK;
}

static HRESULT __stdcall hkCreateIndexBuffer(LPDIRECT3DDEVICE9 pDevice, UINT Length, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DIndexBuffer9 **ppIndexBuffer, HANDLE *pSharedHandle)
{
    OK_TRACE("len: %u", Length);

    auto pBuffer = new FakeIndexBuffer(pDevice, Length, Usage, Format, Pool);
    pBuffer->AddRef();

    *ppIndexBuffer = (IDirect3DIndexBuffer9*)pBuffer;
    return D3D_OK;
}

static HRESULT __stdcall hkSetIndices(LPDIRECT3DDEVICE9 pDevice, IDirect3DIndexBuffer9 *pIndexData)
{
    auto orgFunc = D3D_ok::GetInstance()->m_hkSetInd->getLocation();
    auto& currentIB = D3D_ok::GetInstance()->m_currentIB;

    OK_TRACE("indices = %p | was: %p", pIndexData, currentIB);

    // If a non fake buffer is set, just let the original device handle it.
    if (pIndexData && !D3D_ok::IsFake(pIndexData))
    {
        // But we need to release ours first if it is managed by us.
        if (currentIB != OK_GARBAGE_PTR && currentIB && D3D_ok::IsFake(currentIB))
        {
            ((FakeUnknown*)currentIB)->m_internal->ReleaseInternal();
        }

        currentIB = pIndexData;
        return decltype(&hkSetIndices)(orgFunc)(pDevice, pIndexData);
    }

    auto prev = currentIB;

    // If the previously set buffer is not fake, let the original device handle the internal release.
    // We also need to let it clear if the previous state is unknown.
    if (prev == OK_GARBAGE_PTR || prev && !D3D_ok::IsFake(prev))
    {
        auto hr = decltype(&hkSetIndices)(orgFunc)(pDevice, nullptr);
        if (hr != D3D_OK)
        {
            return hr;
        }
        prev = nullptr;
    }

    if (currentIB = pIndexData)
    {
        ((FakeUnknown*)currentIB)->m_internal->AddRefInternal();
    }
    if (prev)
    {
        ((FakeUnknown*)prev)->m_internal->ReleaseInternal();
    }
    return D3D_OK;
}

static HRESULT __stdcall hkGetIndices(LPDIRECT3DDEVICE9 pDevice, IDirect3DIndexBuffer9 **ppIndexData, UINT *pBaseVertexIndex)
{
    OK_TRACE("");

    auto& currentIB = D3D_ok::GetInstance()->m_currentIB;

    if (currentIB && currentIB != OK_GARBAGE_PTR)
    {
        *ppIndexData = currentIB;
        (*ppIndexData)->AddRef();
        return D3D_OK;
    }

    auto orgFunc = D3D_ok::GetInstance()->m_hkGetInd->getLocation();
    return decltype(&hkGetIndices)(orgFunc)(pDevice, ppIndexData, pBaseVertexIndex);
}


bool D3D_ok::startBuffers(IDirect3DDevice9 *pDevice)
{
    if (!getDummyMem())
    {
        return false;
    }

    auto crvb = m_hooker.hookVT(pDevice, 26, hkCreateVertexBuffer);
    auto crib = m_hooker.hookVT(pDevice, 27, hkCreateIndexBuffer);
    m_hkSetInd = m_hooker.hookVT(pDevice, 104, hkSetIndices);
    m_hkGetInd = m_hooker.hookVT(pDevice, 105, hkGetIndices);
    // TODO: Set/GetStreamSource and ProcessVertices. Works without for test targets, but should be done.

    if (!crvb || !crib || !m_hkSetInd ||
        !m_hkGetInd)
    {
        return false;
    }

    return true;
}