#ifndef D3D_OK_FAKEUNKNOWN_H
#define D3D_OK_FAKEUNKNOWN_H

#include <Windows.h>
#include <cstdint>
#include "Trace.h"


// {5C020414-53B3-46B7-A64B-6F2A0C50A854}
static const IID IID_FakeUnknown =
{ 0x5c020414, 0x53b3, 0x46b7,{ 0xa6, 0x4b, 0x6f, 0x2a, 0xc, 0x50, 0xa8, 0x54 } };

class InternalUnknown;


class FakeUnknown
{
public:
    FakeUnknown(InternalUnknown *internal_);

    STDMETHOD(QueryInterface)(THIS_ REFIID riid, void** ppvObj);
    STDMETHOD_(ULONG, AddRef)(THIS);
    STDMETHOD_(ULONG, Release)(THIS);

    InternalUnknown *m_internal;
    uint32_t m_refCount = 0;

};


class InternalUnknown
{
public:
    InternalUnknown(FakeUnknown *host);

    virtual ULONG AddRefInternal();
    virtual ULONG ReleaseInternal();

    FakeUnknown *m_host;
    uint32_t m_internalRefCount = 0;

};

#endif