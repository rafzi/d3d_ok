#include "FakeTexture.h"
#include "FakeSurface.h"
#include "D3D_ok.h"


FakeTexture::FakeTexture(IDirect3DDevice9 *pDevice, UINT width, UINT height, UINT levels, DWORD usage, D3DFORMAT format, D3DPOOL pool)
    : FakeBaseTexture(new InternalTexture(this), pDevice, levels)
{
    m_fakeSurface = new FakeSurface(pDevice, (IUnknown*)this, width, height, usage, format, pool);
    m_fakeSurface->m_internal = new InternalUnknown(m_fakeSurface);
}


D3DRESOURCETYPE FakeTexture::GetType(THIS)
{
    OK_TRACE("");

    return D3DRTYPE_TEXTURE;
}


HRESULT FakeTexture::GetLevelDesc(THIS_ UINT Level, D3DSURFACE_DESC *pDesc)
{
    OK_TRACE("");

    return m_fakeSurface->GetDesc(pDesc);
}

HRESULT FakeTexture::GetSurfaceLevel(THIS_ UINT Level, IDirect3DSurface9** ppSurfaceLevel)
{
    OK_TRACE("");

    *ppSurfaceLevel = (IDirect3DSurface9*)m_fakeSurface;
    (*ppSurfaceLevel)->AddRef();
    return D3D_OK;
}

HRESULT FakeTexture::LockRect(THIS_ UINT Level, D3DLOCKED_RECT* pLockedRect, CONST RECT* pRect, DWORD Flags)
{
    OK_TRACE("");

    pLockedRect->Pitch = m_fakeSurface->m_pitch;
    pLockedRect->pBits = D3D_ok::GetInstance()->getDummyMem();
    return D3D_OK;
}

HRESULT FakeTexture::UnlockRect(THIS_ UINT Level)
{
    OK_TRACE("");

    return D3D_OK;
}

HRESULT FakeTexture::AddDirtyRect(THIS_ CONST RECT* pDirtyRect)
{
    OK_TRACE("");

    return D3D_OK;
}


InternalTexture::InternalTexture(FakeUnknown *host)
    : InternalUnknown(host)
{
}

ULONG InternalTexture::AddRefInternal()
{
    OK_TRACE("%u -> %u", m_internalRefCount, m_internalRefCount+1);

    auto host = (FakeTexture*)m_host;
    for (UINT i = 0; i < host->m_levels; i++)
    {
        host->m_fakeSurface->m_internal->AddRefInternal();
    }
    return InternalUnknown::AddRefInternal();
}

ULONG InternalTexture::ReleaseInternal()
{
    OK_TRACE("%u -> %u", m_internalRefCount, m_internalRefCount-1);

    auto host = (FakeTexture*)m_host;
    for (UINT i = 0; i < host->m_levels; i++)
    {
        host->m_fakeSurface->m_internal->ReleaseInternal();
    }
    return InternalUnknown::ReleaseInternal();
}