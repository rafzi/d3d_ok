#include "FakeVolume.h"
#include "FakeSurface.h"
#include "D3D_ok.h"


FakeVolume::FakeVolume(IDirect3DDevice9 *pDevice, IUnknown *pContainer, UINT width, UINT height, UINT depth, DWORD usage, D3DFORMAT format, D3DPOOL pool)
    : FakeUnknown(new InternalUnknown(this))
{
    m_pDevice = pDevice;

    m_pContainer = pContainer;
    m_rowPitch = FakeSurface::GetPitchByFormat(format, width);
    m_slicePitch = FakeSurface::GetPitchByFormat(format, width*height);
    m_format = format;
    m_usage = usage;
    m_pool = pool;
    m_width = width;
    m_height = height;
    m_depth = depth;
}


ULONG FakeVolume::AddRef(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount+1);

    if (m_pContainer)
    {
        return m_pContainer->AddRef();
    }

    return FakeUnknown::AddRef();
}

ULONG FakeVolume::Release(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount-1);

    if (m_pContainer)
    {
        return m_pContainer->Release();
    }

    return FakeUnknown::Release();
}


HRESULT FakeVolume::GetDevice(THIS_ IDirect3DDevice9** ppDevice)
{
    OK_TRACE("");

    *ppDevice = m_pDevice;
    (*ppDevice)->AddRef();
    return D3D_OK;
}

HRESULT FakeVolume::SetPrivateData(THIS_ REFGUID refguid, CONST void* pData, DWORD SizeOfData, DWORD Flags)
{
    MessageBox(0, "FakeVolume::SetPrivateData", "Not implemented", 0);
    return D3D_OK;
}

HRESULT FakeVolume::GetPrivateData(THIS_ REFGUID refguid, void* pData, DWORD* pSizeOfData)
{
    MessageBox(0, "FakeVolume::GetPrivateData", "Not implemented", 0);
    return D3D_OK;
}

HRESULT FakeVolume::FreePrivateData(THIS_ REFGUID refguid)
{
    MessageBox(0, "FakeVolume::FreePrivateData", "Not implemented", 0);
    return D3D_OK;
}

HRESULT FakeVolume::GetContainer(THIS_ REFIID riid, void** ppContainer)
{
    OK_TRACE("");

    if (riid == IID_FakeUnknown)
    {
        // For internal use to identify fake objects.
        // Don't write to out pointer and don't AddRef.
        return m_pContainer->QueryInterface(IID_FakeUnknown, nullptr);
    }
    if (!ppContainer)
    {
        return D3DERR_INVALIDCALL;
    }
    if (riid == IID_IDirect3DVolumeTexture9)
    {
        *ppContainer = m_pContainer;
        static_cast<IDirect3DVolumeTexture9*>(*ppContainer)->AddRef();
        return D3D_OK;
    }

    return D3DERR_INVALIDCALL;
}

HRESULT FakeVolume::GetDesc(THIS_ D3DVOLUME_DESC *pDesc)
{
    OK_TRACE("");

    pDesc->Format = m_format;
    pDesc->Type = D3DRTYPE_VOLUME;
    pDesc->Usage = m_usage;
    pDesc->Pool = m_pool;
    pDesc->Width = m_width;
    pDesc->Height = m_height;
    pDesc->Depth = m_depth;
    return D3D_OK;
}

HRESULT FakeVolume::LockBox(THIS_ D3DLOCKED_BOX * pLockedVolume, CONST D3DBOX* pBox, DWORD Flags)
{
    OK_TRACE("");

    pLockedVolume->RowPitch = m_rowPitch;
    pLockedVolume->SlicePitch = m_slicePitch;
    pLockedVolume->pBits = D3D_ok::GetInstance()->getDummyMem();
    return D3D_OK;
}

HRESULT FakeVolume::UnlockBox(THIS)
{
    OK_TRACE("");

    return D3D_OK;
}