#include "FakeShader.h"


FakeShader::FakeShader(IDirect3DDevice9 *pDevice)
    : FakeUnknown(new InternalUnknown(this))
    , m_pDevice(pDevice)
{
}


ULONG FakeShader::AddRef(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount+1);

    auto refCount = InterlockedIncrement(&m_refCount);
    if (refCount == 1)
    {
        m_pDevice->AddRef();
        m_internal->AddRefInternal();
    }
    return refCount;
}

ULONG FakeShader::Release(THIS)
{
    OK_TRACE("%u -> %u", m_refCount, m_refCount-1);

    auto refCount = InterlockedDecrement(&m_refCount);
    if (refCount == 0)
    {
        auto pDevice = m_pDevice;

        m_internal->ReleaseInternal();
        pDevice->Release();
    }
    return refCount;
}


HRESULT FakeShader::GetDevice(THIS_ IDirect3DDevice9** ppDevice)
{
    OK_TRACE("");

    *ppDevice = m_pDevice;
    (*ppDevice)->AddRef();
    return D3D_OK;
}

HRESULT FakeShader::GetFunction(THIS_ void* pData, UINT* pSizeOfData)
{
    OK_TRACE("");

    *pSizeOfData = 1;
    return D3D_OK;
}