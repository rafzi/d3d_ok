#include "FakeBaseTexture.h"
#include "FakeSurface.h"


FakeBaseTexture::FakeBaseTexture(InternalUnknown *internal_, IDirect3DDevice9 *pDevice, UINT levels)
    : FakeResource(internal_, pDevice)
    , m_levels(levels == 0 ? 1 : levels)
{
}


DWORD FakeBaseTexture::SetLOD(THIS_ DWORD LODNew)
{
    OK_TRACE("%lu -> %lu", m_lod, LODNew);

    m_lod = LODNew;
    return LODNew;
}

DWORD FakeBaseTexture::GetLOD(THIS)
{
    OK_TRACE("%lu", m_lod);

    return m_lod;
}

DWORD FakeBaseTexture::GetLevelCount(THIS)
{
    OK_TRACE("%lu", m_levels);

    return m_levels;
}

HRESULT FakeBaseTexture::SetAutoGenFilterType(THIS_ D3DTEXTUREFILTERTYPE FilterType)
{
    OK_TRACE("%lu -> %lu", m_filter, FilterType);

    m_filter = FilterType;
    return D3D_OK;
}

D3DTEXTUREFILTERTYPE FakeBaseTexture::GetAutoGenFilterType(THIS)
{
    OK_TRACE("%lu", m_filter);

    return m_filter;
}

void FakeBaseTexture::GenerateMipSubLevels(THIS)
{
    OK_TRACE("");
}