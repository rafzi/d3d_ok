#include "D3D_ok.h"
#include "FakeShader.h"


static HRESULT __stdcall hkCreateVertexShader(LPDIRECT3DDEVICE9 pDevice, const DWORD *pFunction, IDirect3DVertexShader9 **ppShader)
{
    OK_TRACE("");

    auto pShader = new FakeShader(pDevice);
    pShader->AddRef();

    *ppShader = (IDirect3DVertexShader9*)pShader;
    return D3D_OK;
}

static HRESULT __stdcall hkCreatePixelShader(LPDIRECT3DDEVICE9 pDevice, const DWORD *pFunction, IDirect3DPixelShader9 **ppShader)
{
    OK_TRACE("");

    auto pShader = new FakeShader(pDevice);
    pShader->AddRef();

    *ppShader = (IDirect3DPixelShader9*)pShader;
    return D3D_OK;
}

static HRESULT __stdcall hkSetVertexShader(LPDIRECT3DDEVICE9 pDevice, IDirect3DVertexShader9 *pShader)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkSetPixelShader(LPDIRECT3DDEVICE9 pDevice, IDirect3DPixelShader9 *pShader)
{
    OK_TRACE("");

    return D3D_OK;
}


bool D3D_ok::startShaders(IDirect3DDevice9 *pDevice)
{
    auto crvert = m_hooker.hookVT(pDevice, 91, hkCreateVertexShader);
    auto crpix = m_hooker.hookVT(pDevice, 106, hkCreatePixelShader);
    auto setvs = m_hooker.hookVT(pDevice, 92, hkSetVertexShader);
    auto setps = m_hooker.hookVT(pDevice, 107, hkSetPixelShader);

    if (!crvert || !crpix || !setvs ||
        !setps)
    {
        return false;
    }

    return true;
}