#include "D3D_ok.h"
#include "FakeTexture.h"
#include "FakeVolumeTexture.h"
#include "FakeCubeTexture.h"


static HRESULT __stdcall hkUpdateSurface(LPDIRECT3DDEVICE9 pDevice, IDirect3DSurface9 *pSourceSurface, const RECT *pSourceRect, IDirect3DSurface9 *pDestinationSurface, const POINT *pDestinationPoint)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkUpdateTexture(LPDIRECT3DDEVICE9 pDevice, IDirect3DBaseTexture9 *pSourceTexture, IDirect3DBaseTexture9 *pDestinationTexture)
{
    OK_TRACE("");

    return D3D_OK;
}

static HRESULT __stdcall hkGetTexture(LPDIRECT3DDEVICE9 pDevice, DWORD Stage, IDirect3DBaseTexture9 **ppTexture)
{
    OK_TRACE("stages[%lu]", Stage);

    auto& texStages = D3D_ok::GetInstance()->m_texStages;

    auto itPrev = texStages.find(Stage);
    if (itPrev != texStages.end() && itPrev->second)
    {
        *ppTexture = itPrev->second;
        (*ppTexture)->AddRef();
        return D3D_OK;
    }

    auto orgFunc = D3D_ok::GetInstance()->m_hkGetTex->getLocation();
    return decltype(&hkGetTexture)(orgFunc)(pDevice, Stage, ppTexture);
}

static HRESULT __stdcall hkSetTexture(LPDIRECT3DDEVICE9 pDevice, DWORD Sampler, IDirect3DBaseTexture9 *pTexture)
{
    auto orgFunc = D3D_ok::GetInstance()->m_hkSetTex->getLocation();
    auto& texStages = D3D_ok::GetInstance()->m_texStages;

    OK_TRACE("stages[%lu] = %p", Sampler, pTexture);

    auto itTexStage = texStages.find(Sampler);

    // If a non fake texture is set, just let the original device handle it.
    if (pTexture && !D3D_ok::IsFake(pTexture))
    {
        // But we need to release ours first if it is managed by us.
        if (itTexStage != texStages.end() && itTexStage->second && D3D_ok::IsFake(itTexStage->second))
        {
            ((FakeUnknown*)itTexStage->second)->m_internal->ReleaseInternal();
        }

        texStages[Sampler] = pTexture;
        return decltype(&hkSetTexture)(orgFunc)(pDevice, Sampler, pTexture);
    }

    auto prev = OK_GARBAGE_PTR;

    // If the previously set buffer is not fake, let the original device handle the internal release.
    // We also need to let it clear if the previous state is unknown.
    if (itTexStage == texStages.end() || itTexStage->second && !D3D_ok::IsFake(itTexStage->second))
    {
        auto hr = decltype(&hkSetTexture)(orgFunc)(pDevice, Sampler, nullptr);
        if (hr != D3D_OK)
        {
            return hr;
        }
        prev = nullptr;
    }
    else
    {
        prev = itTexStage->second;
    }

    if (texStages[Sampler] = pTexture)
    {
        ((FakeUnknown*)pTexture)->m_internal->AddRefInternal();
    }
    if (prev)
    {
        ((FakeUnknown*)prev)->m_internal->ReleaseInternal();
    }
    return D3D_OK;
}

static HRESULT __stdcall hkSetDepthStencilSurface(LPDIRECT3DDEVICE9 pDevice, IDirect3DSurface9 *pNewZStencil)
{
    auto orgFunc = D3D_ok::GetInstance()->m_hkSetStencil->getLocation();
    auto& currentStencil = D3D_ok::GetInstance()->m_currentStencil;

    OK_TRACE("stencil = %p | was: %p", pNewZStencil, currentStencil);

    // If a non fake surface is set, just let the original device handle it.
    if (pNewZStencil && !D3D_ok::IsFake(pNewZStencil))
    {
        // But we need to release ours first if it is managed by us.
        if (currentStencil != OK_GARBAGE_PTR && currentStencil && D3D_ok::IsFake(currentStencil))
        {
            ((FakeUnknown*)currentStencil)->m_internal->ReleaseInternal();
        }

        currentStencil = pNewZStencil;
        return decltype(&hkSetDepthStencilSurface)(orgFunc)(pDevice, pNewZStencil);
    }

    auto prev = currentStencil;

    // If the previously set surface is not fake, let the original device handle the internal release.
    // We also need to let it clear if the previous state is unknown.
    if (prev == OK_GARBAGE_PTR || prev && !D3D_ok::IsFake(prev))
    {
        auto hr = decltype(&hkSetDepthStencilSurface)(orgFunc)(pDevice, nullptr);
        if (hr != D3D_OK)
        {
            return hr;
        }
        prev = nullptr;
    }

    if (currentStencil = pNewZStencil)
    {
        ((FakeUnknown*)currentStencil)->m_internal->AddRefInternal();
    }
    if (prev)
    {
        ((FakeUnknown*)prev)->m_internal->ReleaseInternal();
    }
    return D3D_OK;
}

static HRESULT __stdcall hkGetDepthStencilSurface(LPDIRECT3DDEVICE9 pDevice, IDirect3DSurface9 **ppZStencilSurface)
{
    OK_TRACE("");

    auto& currentStencil = D3D_ok::GetInstance()->m_currentStencil;

    if (currentStencil && currentStencil != OK_GARBAGE_PTR)
    {
        *ppZStencilSurface = currentStencil;
        (*ppZStencilSurface)->AddRef();
        return D3D_OK;
    }

    auto orgFunc = D3D_ok::GetInstance()->m_hkGetStencil->getLocation();
    return decltype(&hkGetDepthStencilSurface)(orgFunc)(pDevice, ppZStencilSurface);
}

static HRESULT __stdcall hkCreateTexture(LPDIRECT3DDEVICE9 pDevice, UINT Width, UINT Height, UINT Levels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DTexture9 **ppTexture, HANDLE *pSharedHandle)
{
    OK_TRACE("%u x %u x %u", Width, Height, Levels);

    if (Usage & D3DUSAGE_RENDERTARGET)
    {
        auto orgFunc = D3D_ok::GetInstance()->m_hkCreateTex->getLocation();
        return decltype(&hkCreateTexture)(orgFunc)(pDevice, Width, Height, Levels, Usage, Format, Pool, ppTexture, pSharedHandle);
    }

    auto pTexture = new FakeTexture(pDevice, Width, Height, Levels, Usage, Format, Pool);
    pTexture->AddRef();

    *ppTexture = (IDirect3DTexture9*)pTexture;
    return D3D_OK;
}

static HRESULT __stdcall hkCreateVolumeTexture(LPDIRECT3DDEVICE9 pDevice, UINT Width, UINT Height, UINT Depth, UINT Levels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DVolumeTexture9 **ppVolumeTexture, HANDLE *pSharedHandle)
{
    OK_TRACE("%u x %u x %u x %u", Width, Height, Depth, Levels);

    auto pTexture = new FakeVolumeTexture(pDevice, Width, Height, Depth, Levels, Usage, Format, Pool);
    pTexture->AddRef();

    *ppVolumeTexture = (IDirect3DVolumeTexture9*)pTexture;
    return D3D_OK;
}

static HRESULT __stdcall hkCreateCubeTexture(LPDIRECT3DDEVICE9 pDevice, UINT EdgeLength, UINT Levels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DCubeTexture9 **ppCubeTexture, HANDLE *pSharedHandle)
{
    OK_TRACE("%u x %u x %u", EdgeLength, EdgeLength, Levels);

    auto pTexture = new FakeCubeTexture(pDevice, EdgeLength, Levels, Usage, Format, Pool);
    pTexture->AddRef();

    *ppCubeTexture = (IDirect3DCubeTexture9*)pTexture;
    return D3D_OK;
}


static HRESULT __stdcall hkCheckDepthStencilMatch(LPDIRECT3D9 pD3D, UINT Adapter, D3DDEVTYPE DeviceType, D3DFORMAT AdapterFormat, D3DFORMAT RenderTargetFormat, D3DFORMAT DepthStencilFormat)
{
    OK_TRACE("");

    return D3D_OK;
}


bool D3D_ok::startTextures(IDirect3DDevice9 *pDevice)
{
    if (!getDummyMem())
    {
        return false;
    }

    IDirect3D9 *d3d = NULL;
    if (pDevice->GetDirect3D(&d3d) != D3D_OK || !d3d)
    {
        return false;
    }

    auto updsur = m_hooker.hookVT(pDevice, 30, hkUpdateSurface);
    auto updtex = m_hooker.hookVT(pDevice, 31, hkUpdateTexture);
    m_hkGetTex = m_hooker.hookVT(pDevice, 64, hkGetTexture);
    m_hkSetTex = m_hooker.hookVT(pDevice, 65, hkSetTexture);
    m_hkSetStencil = m_hooker.hookVT(pDevice, 39, hkSetDepthStencilSurface);
    m_hkGetStencil = m_hooker.hookVT(pDevice, 40, hkGetDepthStencilSurface);
    auto stenma = m_hooker.hookVT(d3d, 12, hkCheckDepthStencilMatch);
    m_hkCreateTex = m_hooker.hookVT(pDevice, 23, hkCreateTexture);
    auto crvolu = m_hooker.hookVT(pDevice, 24, hkCreateVolumeTexture);
    auto crcube = m_hooker.hookVT(pDevice, 25, hkCreateCubeTexture);

    d3d->Release();

    if (!updsur || !updtex || !m_hkGetTex ||
        !m_hkSetTex || !m_hkSetStencil || !m_hkGetStencil ||
        !stenma || !m_hkCreateTex || !crvolu ||
        !crcube)
    {
        return false;
    }

    return true;
}