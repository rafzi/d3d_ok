#include "main.h"
#include "D3D_ok.h"
#include "hacklib/PatternScanner.h"
#include "hacklib/D3DDeviceFetcher.h"
#include "hacklib/Hooker.h"
#include <thread>
#include <chrono>


class DummyWindow
{
public:
    ~DummyWindow();
    HWND create();
private:
    HWND m_hWNd = NULL;
};


hl::StaticInit<TestDll> g_initObj;
hl::Hooker *g_pHooker;
void *g_dummyMem;
std::map<IDirect3DDevice9*, bool> g_oked;



void hkDevAddRef(hl::CpuContext *ctx)
{
#ifdef ARCH_64BIT
    auto dev = (IDirect3DDevice9*)(ctx->RCX);
#else
    auto dev = *(IDirect3DDevice9**)(ctx->ESP + 4);
#endif

    if (!g_oked[dev])
    {
        auto k = new D3D_ok;
        k->setDummyMem(g_dummyMem);
        if (!k->startAll(dev))
        {
            printf("didnt hook created device\n");
        }
        else
        {
            printf("hooked created device\n");
        }

        g_oked[dev] = true;
    }
}


TestDll *GetMain()
{
    return g_initObj.getMain();
}


bool TestDll::init()
{
    AllocConsole();
    FILE *fh;
    freopen_s(&fh, "CONOUT$", "wb", stdout);

    try
    {
        hl::PatternScanner scanner;
        auto results = scanner.find({ "!s_dxContext" });
        void *dxContext = *(void**)(results[0] - 0x11);
        printf("s_dxContext: %p\n", results[0]);
    }
    catch (...)
    {
        printf("error while finding patterns\n");
    }


    g_dummyMem = D3D_ok::AllocDummyMem(50000000);
    if (!g_dummyMem)
    {
        printf("alloc failed\n");
    }

    D3D_ok ok;
    ok.setDummyMem(g_dummyMem);

    // Find existing device.
    /*auto pDevice = hl::D3DDeviceFetcher::GetD3D9Device();
    if (!pDevice || !ok.startAll(pDevice))
    {
        printf("didnt hook fetched device\n");
    }*/

    // Find AddRef address.
    DummyWindow wnd;
    auto hWnd = wnd.create();
    auto d3d = Direct3DCreate9(D3D_SDK_VERSION);
    D3DPRESENT_PARAMETERS d3dPar = { 0 };
    d3dPar.Windowed = TRUE;
    d3dPar.SwapEffect = D3DSWAPEFFECT_DISCARD;
    IDirect3DDevice9 *testDev = nullptr;
    d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dPar, &testDev);
    auto addRef = ((uintptr_t**)testDev)[0][1];
    testDev->Release();
    d3d->Release();

    // Hook AddRef to get every d3d object for CreateDevice.
    hl::Hooker hooker;
    g_pHooker = &hooker;
    hooker.hookDetour(addRef, 5, hkDevAddRef);

    // Test
    /*MessageBox(0, 0, 0, 0);
    IDirect3DDevice9 *testDev = nullptr;
    d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, 0, 0, 0, &testDev);

    d3d->Release();*/


    //while (GetAsyncKeyState(VK_HOME) >= 0)
    while (true)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }

    printf("mainloop done\n");

    FreeConsole();

    return false;
}