#ifndef D3D_OK_H
#define D3D_OK_H

#include "hacklib/Hooker.h"
#include <d3d9.h>
#include <unordered_map>


static void* const OK_GARBAGE_PTR = (void*)0xdeadbeef;


class D3D_ok
{
public:
    static D3D_ok *GetInstance();
    static void *AllocDummyMem(size_t size);
    static void FreeDummyMem(void *ptr);

    static bool IsFake(IUnknown *pUnknown);

public:
    D3D_ok();

    void setDummyMem(void *ptr);
    void *getDummyMem() const;

    bool startAll(IDirect3DDevice9 *pDevice);
    bool startDrawing(IDirect3DDevice9 *pDevice);
    bool startTextures(IDirect3DDevice9 *pDevice);
    bool startBuffers(IDirect3DDevice9 *pDevice);
    bool startShaders(IDirect3DDevice9 *pDevice);

    // General
    const hl::IHook *m_hkReset = nullptr;

    // Textures
    const hl::IHook *m_hkCreateTex = nullptr;
    const hl::IHook *m_hkGetTex = nullptr;
    const hl::IHook *m_hkSetTex = nullptr;
    const hl::IHook *m_hkSetStencil = nullptr;
    const hl::IHook *m_hkGetStencil = nullptr;
    IDirect3DSurface9 *m_currentStencil = (IDirect3DSurface9*)OK_GARBAGE_PTR;
    std::unordered_map<DWORD, IDirect3DBaseTexture9*> m_texStages;

    // Buffers
    const hl::IHook *m_hkSetInd = nullptr;
    const hl::IHook *m_hkGetInd = nullptr;
    IDirect3DIndexBuffer9 *m_currentIB = (IDirect3DIndexBuffer9*)OK_GARBAGE_PTR;

private:
    hl::Hooker m_hooker;
    IDirect3DDevice9 *m_pDevice = nullptr;
    void *m_dummyMem = nullptr;

};

#endif