#include "FakeCubeTexture.h"
#include "FakeSurface.h"
#include "D3D_ok.h"


FakeCubeTexture::FakeCubeTexture(IDirect3DDevice9 *pDevice, UINT edgeLength, UINT levels, DWORD usage, D3DFORMAT format, D3DPOOL pool)
    : FakeBaseTexture(new InternalCubeTexture(this), pDevice, levels)
{
    m_fakeSurface = new FakeSurface(pDevice, (IUnknown*)this, edgeLength, edgeLength, usage, format, pool);
    m_fakeSurface->m_internal = new InternalUnknown(m_fakeSurface);
}


D3DRESOURCETYPE FakeCubeTexture::GetType(THIS)
{
    OK_TRACE("");

    return D3DRTYPE_CUBETEXTURE;
}


HRESULT FakeCubeTexture::GetLevelDesc(THIS_ UINT Level, D3DSURFACE_DESC *pDesc)
{
    OK_TRACE("");

    return m_fakeSurface->GetDesc(pDesc);
}

HRESULT FakeCubeTexture::GetCubeMapSurface(THIS_ D3DCUBEMAP_FACES FaceType, UINT Level, IDirect3DSurface9** ppCubeMapSurface)
{
    OK_TRACE("");

    *ppCubeMapSurface = (IDirect3DSurface9*)m_fakeSurface;
    (*ppCubeMapSurface)->AddRef();
    return D3D_OK;
}

HRESULT FakeCubeTexture::LockRect(THIS_ D3DCUBEMAP_FACES FaceType, UINT Level, D3DLOCKED_RECT* pLockedRect, CONST RECT* pRect, DWORD Flags)
{
    OK_TRACE("");

    pLockedRect->Pitch = m_fakeSurface->m_pitch;
    pLockedRect->pBits = D3D_ok::GetInstance()->getDummyMem();
    return D3D_OK;
}

HRESULT FakeCubeTexture::UnlockRect(THIS_ D3DCUBEMAP_FACES FaceType, UINT Level)
{
    OK_TRACE("");

    return D3D_OK;
}

HRESULT FakeCubeTexture::AddDirtyRect(THIS_ D3DCUBEMAP_FACES FaceType, CONST RECT* pDirtyRect)
{
    OK_TRACE("");

    return D3D_OK;
}


InternalCubeTexture::InternalCubeTexture(FakeUnknown *host)
    : InternalUnknown(host)
{
}

ULONG InternalCubeTexture::AddRefInternal()
{
    OK_TRACE("%u -> %u", m_internalRefCount, m_internalRefCount+1);

    auto host = (FakeCubeTexture*)m_host;
    for (UINT i = 0; i < 6*host->m_levels; i++)
    {
        host->m_fakeSurface->m_internal->AddRefInternal();
    }
    return InternalUnknown::AddRefInternal();
}

ULONG InternalCubeTexture::ReleaseInternal()
{
    OK_TRACE("%u -> %u", m_internalRefCount, m_internalRefCount-1);

    auto host = (FakeCubeTexture*)m_host;
    for (UINT i = 0; i < 6*host->m_levels; i++)
    {
        host->m_fakeSurface->m_internal->ReleaseInternal();
    }
    return InternalUnknown::ReleaseInternal();
}