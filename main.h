#ifndef D3DOK_MAIN_H
#define D3DOK_MAIN_H

#include "hacklib/Main.h"
#include "hacklib/ConsoleEx.h"


class TestDll : public hl::Main
{
public:
    bool init() override;

private:
    hl::ConsoleEx m_con;

};


TestDll *GetMain();


#endif