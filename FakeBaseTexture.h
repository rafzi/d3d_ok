#ifndef D3D_OK_FAKEBASETEXTURE_H
#define D3D_OK_FAKEBASETEXTURE_H

#include "FakeResource.h"


class FakeBaseTexture : public FakeResource
{
public:
    FakeBaseTexture(InternalUnknown *internal_, IDirect3DDevice9 *pDevice, UINT levels);

    STDMETHOD_(DWORD, SetLOD)(THIS_ DWORD LODNew);
    STDMETHOD_(DWORD, GetLOD)(THIS);
    STDMETHOD_(DWORD, GetLevelCount)(THIS);
    STDMETHOD(SetAutoGenFilterType)(THIS_ D3DTEXTUREFILTERTYPE FilterType);
    STDMETHOD_(D3DTEXTUREFILTERTYPE, GetAutoGenFilterType)(THIS);
    STDMETHOD_(void, GenerateMipSubLevels)(THIS);

    DWORD m_lod = 0;
    DWORD m_levels;
    D3DTEXTUREFILTERTYPE m_filter = D3DTEXF_NONE;

};

#endif