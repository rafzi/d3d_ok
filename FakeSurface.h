#ifndef D3D_OK_FAKESURFACE_H
#define D3D_OK_FAKESURFACE_H

#include "FakeResource.h"


class FakeSurface : public FakeResource
{
public:
    static INT GetPitchByFormat(D3DFORMAT format, UINT width);

public:
    FakeSurface(IDirect3DDevice9 *pDevice, IUnknown *pContainer, UINT width, UINT height, DWORD usage, D3DFORMAT format, D3DPOOL pool);

    STDMETHOD_(ULONG, AddRef)(THIS) override;
    STDMETHOD_(ULONG, Release)(THIS) override;

    STDMETHOD_(D3DRESOURCETYPE, GetType)(THIS) override;

    STDMETHOD(GetContainer)(THIS_ REFIID riid, void** ppContainer);
    STDMETHOD(GetDesc)(THIS_ D3DSURFACE_DESC *pDesc);
    STDMETHOD(LockRect)(THIS_ D3DLOCKED_RECT* pLockedRect, CONST RECT* pRect, DWORD Flags);
    STDMETHOD(UnlockRect)(THIS);
    STDMETHOD(GetDC)(THIS_ HDC *phdc);
    STDMETHOD(ReleaseDC)(THIS_ HDC hdc);

    IUnknown *m_pContainer;
    INT m_pitch;
    D3DFORMAT m_format;
    DWORD m_usage;
    D3DPOOL m_pool;
    UINT m_width;
    UINT m_height;

};

#endif